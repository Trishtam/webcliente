﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using WebCliente.Models.ViewModel;

namespace WebCliente.Controllers
{
    public class UsuarioController : Controller
    {
        /// <summary>
        /// Pre Carga de formulario de gestion de usuarios.
        /// </summary>
        /// <returns></returns>
        public ActionResult Ingresar()
        {
            usuarios usuario = new usuarios();
            usuario = fillUsr();
            return View(usuario);
        }

        /// <summary>
        /// Método para llamar servicio de ingresar usuarios.
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Ingresar(FormCollection usuario)
        {
            usuarios RespUsr = new usuarios();
            try
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }

                var client = new RestClient(ConfigurationManager.AppSettings["Endpoint_UsuarioN"]);
                var req = new RestRequest(Method.POST);
                usuarios usuReq = new usuarios();

                usuReq.user = usuario["user"];
                usuReq.pass = usuario["pass"];
                
                req.AddJsonBody(usuReq);
                var res = client.Execute(req);
                usuarios resPr = JsonConvert.DeserializeObject<usuarios>(res.Content);

                RespUsr = fillUsr();
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (resPr.respuesta != "Usuario Ingresado.")
                    {
                        RespUsr.respuesta = resPr.respuesta;
                    }
                    else
                    {
                        RespUsr.respuesta = "Ok"; 
                    }

                }
                if (res.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    RespUsr.respuesta = resPr.respuesta;
                }
                return View("Ingresar", RespUsr);

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        /// <summary>
        /// Metodo privado para traer data de usuarios registrados.
        /// </summary>
        /// <returns></returns>
        private usuarios fillUsr()
        {
            usuarios objUsu = new usuarios();
            try
            {

                var client = new RestClient(ConfigurationManager.AppSettings["Endpoint_Usuario"]);
                var req = new RestRequest(Method.GET);
                var res = client.Execute(req);

                List<usuarios> lstUsu = JsonConvert.DeserializeObject<List<usuarios>>(res.Content);
                objUsu.lstUsuarios = lstUsu;
                return objUsu;

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
    }
}