﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCliente.Models.ViewModel;

namespace WebCliente.Controllers
{
    public class PersonaController : Controller
    {
        /// <summary>
        /// Pre Carga de formulario de gestion de usuarios.
        /// </summary>
        /// <returns></returns>
        public ActionResult Ingresar()
        {
            persona persona = new persona();
            persona = fillAll();
            return View(persona);
        }
        /// <summary>
        /// Método para llamar servicio de ingresar usuarios.
        /// </summary>
        /// <param name="pers"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Ingresar(FormCollection persona)
        {
            persona RespPer = new persona();
            try
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }
                
                var client = new RestClient(ConfigurationManager.AppSettings["Endpoint_PersonaN"]);
                var req = new RestRequest(Method.POST);
                persona perReq = new persona();

                perReq.Prim_Nombre = persona["Prim_Nombre"];
                perReq.Sec_Nombre = persona["Sec_Nombre"];
                perReq.Prim_Apellido = persona["Prim_Apellido"];
                perReq.Sec_Apellido = persona["Sec_Apellido"];
                perReq.TipoIdentificacion = persona["TipoIdentificacion"];
                perReq.NumeroIdentificacion = persona["NumeroIdentificacion"];
                perReq.Email = persona["Email"];
                req.AddJsonBody(perReq);
                var res = client.Execute(req);
                persona resPr = JsonConvert.DeserializeObject<persona>(res.Content);
                RespPer = fillAll();
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (resPr.respuesta!= "Ingresado.")
                    {
                        RespPer.respuesta = resPr.respuesta;
                    }
                    else
                    {   
                        RespPer.respuesta = "Ok";
                    }
                    
                }
                if (res.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    RespPer.respuesta = resPr.respuesta;
                }
                return View("Ingresar", RespPer);

            }
            catch (Exception ex)
            {

                throw ex;
            }

            
        }

        /// <summary>
        /// Metodo privado para traer data de personas registradas y cargar el dropdown de tipo de documento.
        /// </summary>
        /// <returns></returns>
        private persona fillAll()
        {
            persona objPer = new persona();
            try
            {

                var client = new RestClient(ConfigurationManager.AppSettings["Endpoint_Persona"]);                
                var req = new RestRequest(Method.GET);
                var res = client.Execute(req);

                List<persona> lstPer = JsonConvert.DeserializeObject<List<persona>>(res.Content);
                objPer.lstPersona = lstPer;

                //Creo el dropdownlist quemado, por que no hay instruccion de enviarlo por base de datos.
                #region Dropdow list tipo de documento
                List<SelectListItem> ddTD = new List<SelectListItem>() {
                    new SelectListItem {
                            Text = "Seleccione...", Value = null
                        },
                        new SelectListItem {
                            Text = "Cédula de Ciudananía.", Value = "CC"
                        },
                        new SelectListItem {
                            Text = "Cédula de Extranjería.", Value = "CE"
                        },
                        new SelectListItem {                            
                            Text = "NUIP.", Value = "NUIP"
                        },
                        new SelectListItem {
                            Text = "Pasaporte.", Value = "PS"
                        },
                        new SelectListItem {
                            Text = "Permiso de Residencia.", Value = "PR"
                        },
                        new SelectListItem {
                            Text = "Tarjeta de Identidad.", Value = "TI"
                        },

                    };
                ViewBag.TipoDoc = ddTD;
                #endregion

                return objPer;

            }
            catch (Exception ex)
            {

                throw ex;
            }

            
        }

    }
}