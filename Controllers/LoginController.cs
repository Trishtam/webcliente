﻿using RestSharp;
using System;
using System.Configuration;
using System.Web.Mvc;
using WebCliente.Models.ViewModel;

namespace WebCliente.Controllers
{
    public class LoginController : Controller
    {

        public ActionResult Index()
        {
            login test = new login();
            test.respuesta = "New";
            return View(test);
        }
        // GET: Login
        [HttpPost]
        public  ActionResult Index(login logi)
        {
            login resp = new login();
            try
            {   
                var client = new RestClient(ConfigurationManager.AppSettings["Endpoint_Login"]);
                var req = new RestRequest(Method.POST);
                login logReq = new login();
                logReq.user = logi.user;
                logReq.pass = logi.pass;
                req.AddJsonBody(logReq);
                var res = client.Execute(req);               

                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    resp.respuesta = "Ok";                    
                    return View(resp);
                }
                if (res.StatusCode==System.Net.HttpStatusCode.Unauthorized)
                {
                    resp.respuesta = res.Content;
                }
                
            }
            catch (Exception ex)
            {

                throw new Exception("Problemas al Conectar");
            }
            return View(resp);

        }
    }
}