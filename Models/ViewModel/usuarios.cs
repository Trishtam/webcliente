﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebCliente.Models.ViewModel
{
    public class usuarios
    {
        [Required(ErrorMessage ="Se requiere este campo.")]
        public string user { get; set; }

        [Required(ErrorMessage = "Se requiere contraseña.")]
        public string pass { get; set; }

        public string passComp { get; set; }
        public string respuesta { get; set; }
        public List<usuarios> lstUsuarios { get; set; }
    }

    
}