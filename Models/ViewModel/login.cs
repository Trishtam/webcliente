﻿using System.ComponentModel.DataAnnotations;

namespace WebCliente.Models.ViewModel
{
    public class login
    {
        [Required(ErrorMessage = "Se requiere usuario.")]
        public string user { get; set; }
        [Required(ErrorMessage = "Se requiere password.")]
        public string pass { get; set; }

        public string respuesta { get; set; }
    }
}