﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebCliente.Models.ViewModel
{
    public class persona
    {
        public int id_Persona { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre.")]
        public string Prim_Nombre { get; set; }
        public string Sec_Nombre { get; set; }

        [Required(ErrorMessage = "Se requiere Apellido.")]
        public string Prim_Apellido { get; set; }
        public string Sec_Apellido { get; set; }

        [Required(ErrorMessage = "Se requiere número de identificación.")]
        public string NumeroIdentificacion { get; set; }

        [Required(ErrorMessage = "El correo es necesario.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Se requiere el tipo de identificación.")]
        public string TipoIdentificacion { get; set; }
        public string FechaCreacion { get; set; }
        public string Concat_Identificacion { get; set; }
        public string Concat_Nombres { get; set; }

        public string respuesta { get; set; }
        public List<persona> lstPersona { get; set; }

        
    }


   
}